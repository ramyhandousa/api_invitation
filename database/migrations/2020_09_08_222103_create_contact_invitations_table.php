<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_invitations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type_user',['guest','supervisor']);
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('invitation_id');
            $table->string('name',50);
            $table->string('phone',20);
            $table->foreign('invitation_id')->references('id')->on('invitations')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_invitations');
    }
}
