<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('permission_guest_id')->nullable();
            $table->unsignedBigInteger('contact_invitation_id');
            $table->string('value');
            $table->foreign('permission_guest_id')->references('id')->on('permission_guests')->onDelete('cascade');
            $table->foreign('contact_invitation_id')->references('id')->on('contact_invitations') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_contacts');
    }
}
