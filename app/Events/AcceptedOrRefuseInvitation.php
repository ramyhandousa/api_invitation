<?php

namespace App\Events;

class AcceptedOrRefuseInvitation extends Event
{

    public $user;
    public $invitation;
    public $status;
    public function __construct($user,$invitation,$status)
    {

        $this->user = $user;
        $this->invitation = $invitation;
        $this->status = $status;
    }

}
