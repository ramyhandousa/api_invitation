<?php

namespace App\Events;

class phoneChange extends Event
{
    public $user;
    public $code ;
    public $request ;

    public function __construct( $user , $code ,  $request)
    {
        $this->user     = $user;
        $this->code     = $code;
        $this->request  = $request;
    }
}
