<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class OrderOffer extends Model
{
    protected $guarded = [];

    public $timestamps = false;


    public function order(){
        return $this->belongsTo(Order::class,'order_id');

    }

    public function provider()
    {
        return $this->belongsTo(User::class,'provider_id');
    }

    public function bank_transfer(){
        return $this->hasMany(BankTransfer::class,'offer_id');
    }

}
