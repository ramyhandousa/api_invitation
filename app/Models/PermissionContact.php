<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionContact extends Model
{
    protected $guarded = [];


    public $timestamps = false;
}
