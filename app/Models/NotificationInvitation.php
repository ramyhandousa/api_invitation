<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class NotificationInvitation extends Model
{

    protected $guarded = [];
    public function user(){
        return $this->belongsTo(User::class)->select('id','name');
    }

    public function notification(){
        return $this->belongsTo(Notification::class);
    }


}
