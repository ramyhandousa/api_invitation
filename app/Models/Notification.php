<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Notification extends Model
{
    protected $fillable = [ 'user_id','sender_id','invitation_id' ,'title','body','type','is_read'];

    public function user()
    {

        return $this->belongsTo(User::class);
    }

    public function invitation()
    {
        return $this->belongsTo(Invitation::class);
    }


    public function notify_invitation(){
        return $this->hasMany(NotificationInvitation::class);
    }


    public function notify_invitation_last(){
//        return   $this->notify_invitation()->latest('id')->where('status','!=',0)->take(2);
        return   $this->notify_invitation()->latest('id')->take(2);
    }


    public function skip_invitation_last(){
//        return   $this->notify_invitation()->latest('id')->where('status','!=',0)->skip(2)->take(500);
        return   $this->notify_invitation()->latest('id')->skip(2)->take(500);
    }

    public function notify_invitation_un_read(){
//        return   $this->skip_invitation_last()->where('is_read','=',0);
        return   $this->notify_invitation_last()->where('is_read','=',0);
    }


    public function my_status(){
        return $this->hasMany(NotificationInvitation::class)->where('user_id', Auth::id())->first();
    }


}
