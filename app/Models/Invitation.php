<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $guarded = [];


    public static function boot() {
        parent::boot();
        static::creating(function ($order) {
//            $order->payment_money = '0';
//            $order->payment = 'bank_transfer';
        });
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }
    public function contact(){
        return $this->hasMany(ContactInvitation::class);
    }

    public function guest_contact(){
        return $this->hasMany(ContactInvitation::class)->where('type_user','=','guest');
    }

    public function supervisor_contact(){
        return $this->hasMany(ContactInvitation::class)->where('type_user','=','supervisor');
    }
}
