<?php

namespace App\Models;

use App\Casts\Json;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
       'defined_order', 'user_id', 'provider_id' , 'description' ,'images' , 'app_percentage' ,'is_review',
        'is_pay' ,'time_out','total_price','batches_remaining' ,'payment' ,'paymentId','next_transfer' ,'status',
        'created_at'
    ];

    protected $casts = [
//        'images' => Json::class,
        'images' => 'array',
    ];

    protected $appends = ['status_translation'];


    public static function boot() {
        parent::boot();
        static::creating(function ($order) {
            $order->status = 'pending';
        });
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function provider()
    {
        return $this->belongsTo(User::class,'provider_id');
    }

    public function conversation(){
        return $this->hasOne(Conversation::class);
    }


    public function offers()
    {
        return $this->hasMany(OrderOffer::class,'order_id');
    }

    public function hasOfferPriceByThisProvider()
    {
        if($this->offers()->where('provider_id',auth()->id())->whereNull('price')->count() > 0) return true;
        else return false;
    }

    public function hasOfferNoPrice()
    {
        return $this->offers()->whereNotNull('price')  ;
    }


    public function offerByThisProvider()
    {
        return $this->offers()->where('provider_id',auth()->id())->first();
    }

    public function getStatusTranslationAttribute()
    {
        if ($this->status){
            $status = $this->attributes['status'];

            switch ($status) {
                case 'pending':
                    return  __('global.pending_order');
                    break;
                case 'empty':
                    return  'لم يتم تلقي اي عروض ';
                    break;
                case 'accepted':
                    return  __('global.accepted_order');
                    break;
                case 'refuse':
                    return __('global.refuse_order');
                    break;
                case 'finish':
                    return __('global.finish_order');
                    break;
                default:
                    return '';
            }
        }

    }
}
