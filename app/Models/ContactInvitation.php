<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ContactInvitation extends Model
{
    protected $guarded = [];

    public $timestamps = false;


    public function user(){
        return $this->belongsTo(User::class,);
    }

    public function invitation(){

        return $this->belongsTo(Invitation::class,'invitation_id');
    }

}
