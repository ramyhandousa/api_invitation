<?php


namespace App\Models\Collections;


use App\Http\Resources\User\UserImageResource;
use App\User;
use Illuminate\Database\Eloquent\Collection;

class ConversationCustomCollection extends Collection
{

    public function indexData(){

        return  $this->map(function ($conversation){

//            $last_user_send_msg = $conversation->last_messages->whereNotNull('message')->first();
//            $last_user =  $last_user_send_msg  ? User::find($last_user_send_msg->user_id) : null;
            $sender = $conversation->users()->where('id','!=',auth()->id())->first();


            return [
                'id'            =>  $conversation->id,
                'message'       =>   $this->getLastMessage($conversation,$sender),
//                'user'          =>  $last_user ?  new UserImageResource($last_user) : new UserImageResource($sender) ,
                'user'          =>    new UserImageResource($sender) ,
                'open'        =>  $conversation->status == 'open' ,
                'created_at'     =>   $this->getArabicMonth( $conversation->created_at),
            ];
        })->reverse()->values();
    }

    function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $year = date("Y", strtotime($data));

        $month = $months[$month];

        return $day . ' ' . $month ;
    }

    function getLastMessage($conversation , $sender){
        if ( count($conversation->last_messages) > 0 ){

//            $message = $conversation->last_messages->whereNotNull('message')->where('user_id',$sender->id);
            $message = $conversation->last_messages->whereNotNull('message');

            return  collect($message)->values()->first()['message'];

        }else{
            return  $sender->name .'  يمكنك بدا المحادثة الأن مع ' ;
        }
    }
}
