<?php

namespace App\Policies;

use App\Models\Invitation;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Auth;

class InvitationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function update_invitationPolicy(User $user, Invitation $invitation)
    {
        // Supervisor Can Edit
        $can_edit =  $invitation->supervisor_contact->where('user_id' , Auth::id())->where('edit_invitation',1)->first();

        return ( $user->id === $invitation->user_id || $can_edit )
            ? Response::allow()
            : Response::deny('تأكد من انك الشخص المستحق   ');
    }

    public function update_invitationBySupervisor(User $user, Invitation $invitation)
    {
        // Supervisor Can Edit
        $can_edit =  $invitation->supervisor_contact->where('user_id' , Auth::id())->where('add_invitees',1)->first();

        return ( $user->id === $invitation->user_id || $can_edit )
            ? Response::allow()
            : Response::deny('تأكد من انك لديك الصلاحيات  ');
    }

    public function bank_transfer(User $user, Invitation $invitation)
    {

        return ( $user->id === $invitation->user_id )
            ? Response::allow()
            : Response::deny('تأكد من انك الشخص المستحق للتحويل علي هذه الدعوة  ');
    }
}
