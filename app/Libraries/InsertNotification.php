<?php
namespace App\Libraries;

use App\Libraries\PushNotification;
use App\Models\Device;
use  App\Models\Notification;

class InsertNotification {

    public $push;
    function __construct(PushNotification $push)
    {
        $this->push = $push;
    }

    public  function NotificationDbType($type  ,$user,  $sender = null ,  $request = null  , $order = null ,$additional = null){

            switch($type){

            case $type == 1:

                $data = Notification::create([
                    'user_id' => $user ,
                    'sender_id' => 1,
                    'title' =>   trans('global.connect_us')  ,
                    'body' => $request ,
                    'type' => 1,
                ]);

                return $data;

             break;

            case $type == 2:

                // contactUs Form Users
                $data =  Notification::create([
                    'user_id' => 1 ,
                    'sender_id' => $sender ,
                    'title' => trans('global.connect_us'),
                    'body' => $request ,
                    'type' => 2,
                ]);

                return  $data;

                 break;

            case $type == 3:

                 $data =  Notification::create([
                    'user_id' => $user->id ,
                    'sender_id' => $sender->id,
                    'order_id' => $order->id,
                    'product_id' => $request,
                    'title' =>   'طلبات المنتجات '  ,
                    'body' =>   ' لديك طلب منتج جديد من المستخدم ' .   $sender->name   ,
                    'type' => 3,
                ]);

               return  $data;

             break;

            case $type == 4:

                $data = Notification::create([
                    'user_id' => $user->id ,
                    'sender_id' => $sender->id,
                    'invitation_id' => $order->id,
                    'title' =>   'طلبات الدعوات'  ,
                    'body' =>  ' تم قبول طلب الدعوة من ' .   $sender->name  ,
                    'type' => 4,
                ]);

               return  $data;

             break;

            case $type == 5:

                $data = Notification::create([
                    'user_id' => $user->id ,
                    'sender_id' => $sender->id,
                    'invitation_id' => $order->id,
                    'title' =>   'طلبات الدعوات'  ,
                    'body' =>  ' تم رفض طلب الدعوة من ' .   $sender->name  ,
                    'type' => 5,
                ]);

               return  $data;

             break;

            case $type == 6:

                $data = Notification::create([
                    'user_id'       => $user->id ,
                    'sender_id'     => $sender->id,
                    'order_id'      => $order->id,
                    'title'         =>   ' طلبات المشاريع  '  ,
                    'body'          =>  ' يوجد طلب مشروع جديد من ' .   $sender->name ,
                    'type'          => 6,
                 ]);

                return  $data;
             break;

             case $type == 7:

                $data = Notification::create([
                    'user_id'       => $user ,
                    'sender_id'     => $sender->id,
                    'order_id'      => $order->id,
                    'offer_id'      => $request->id,
                    'title'         =>   'العروض '  ,
                    'body'          =>     $order->id .' علي المشروع رقم  '. $sender->name .' تم قبول عرضك من المستخدم   '  ,
                    'type'          => 7,
                ]);

                 return  $data;

              break;

             case $type == 8:

                $data = Notification::create([
                    'user_id'       => $user ,
                    'sender_id'     => 1,
                    'order_id'      => $order->id,
                    'title'         =>   'العروض '  ,
                    'body'          =>    ' تم الإنتهاء من الوقت المحدد لتلقي العروض  علي طلبك رقم  ' .$order->id  ,
                    'type'          => 8,
                ]);
                 return  $data;

             break;

            case $type == 11:

                $data = Notification::create([
                    'user_id' => $user->id ,
                    'sender_id' => $sender->id,
                    'order_id' => $order->id,
                    'title' =>   'الطلبات  '  ,
                    'body' =>  ' تم تقيم الطلب من المستخدم' .   $sender->name   ,
                    'type' => 11,
                ]);

                return  $data;
                break;

            case $type == 20:
                    $data = Notification::create([
                        'user_id'      => 1,
                        'sender_id'    => $user->id,
                        'order_id'    => $order->id,
                        'title'        =>   'التحويلات'  ,
                        'body'         =>  '  يوجد  تحويل  بنكي من المستخدم   ' .$user->name ,
                        'type'         => 20,
                    ]);
                    return  $data;
                break;

            case $type == 21:
                    $data = Notification::create([
                        'user_id'      => $user->id,
                        'sender_id'    => 1,
                        'order_id'     => $order->id,
                        'title'        =>   'التحويلات'  ,
                        'body'         =>  ' تم قبول التحويل البنكي الخاص بك '  ,
                        'type'         => 21,
                    ]);
                    return  $data;
                break;

            case $type == 22:
                    $data = Notification::create([
                        'user_id'      => $user->id,
                        'sender_id'    => 1,
                        'order_id'     => $order->id,
                        'title'        =>   'التحويلات'  ,
                        'body'         =>  ' تم رفض التحويل البنكي الخاص بسب ' . $request ,
                        'type'         => 22,
                    ]);
                    return  $data;
                break;


            case $type == 23:
                $data = Notification::create([
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'order_id'     => $order->id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم قبول التحويل الخاص بك علي الطلب رقم ' .    $order->id ,
                    'type'         => 23,
                ]);
                return  $data;
            break;

            case $type == 24:
                $data = Notification::create([
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'order_id'     => $order->id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم رفض التحويل علي طلبك من المنتج  رقم ' .    $order->id  .' بسبب  ' . $request,
                    'type'         => 24,
                ]);
                 return  $data;
            break;

            case $type == 25:
                $data = Notification::create([
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'order_id'     => $order->id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم رفض التحويل علي طلبك من المشروع  رقم ' .    $order->id  .' بسبب  ' . $request,
                    'type'         => 25,
                ]);
                return  $data;
            break;

            default:

     }


    }


    private function insertData($data)
    {
     if (count($data) > 0) {

         $time = ['created_at' => now(),'updated_at' => now()];
           Notification::insert($data   + $time);

     }
    }


}

