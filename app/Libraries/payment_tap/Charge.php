<?php

/**
 * Created by PhpStorm.
 * User: RAMY
 * Date: 2/24/2020
 * Time: 6:40 PM
 */


namespace App\Libraries\payment_tap;

use Illuminate\Support\Facades\Validator;

class Charge extends AbstractService
{

    protected $endpoint = 'charges/';
    protected $method = 'post';
    protected $attributes = [];
    protected $token;

    public function __construct( $id = null )
    {
        if ( $id ) {
            $this->attributes['id'] = $id;
            $this->setEndpoint( $id );
        }

        $this->token = 'sk_live_SxW7el2OTJ90DCkg3KcqawVn';
//        $this->token = 'sk_test_IuhcaB1sO9TibDfjzGrYwLqC';

        parent::__construct();
    }


    protected function setEndpoint( $endpoint )
    {
        $this->endpoint .= $endpoint;
    }


    public function setAmount( $amount )
    {
        $this->attributes['amount'] = $amount;
    }

    public function setChargeId( $charge_id )
    {
        $this->attributes['charge_id'] = $charge_id;
    }

    public function setReason( $reason )
    {
        $this->attributes['reason'] = $reason;
    }


    public function setCurrency( $currency )
    {
        $this->attributes['currency'] = $currency;
    }


    public function setThreeDSecure( $threeDSecure )
    {
        $this->attributes['threeDSecure'] = $threeDSecure;
    }


    public function setSave_card( $save_card )
    {
        $this->attributes['save_card'] = $save_card;
    }


    public function setDescription( $description )
    {
        $this->attributes['description'] = $description;
    }


    public function setCustomerName( $name )
    {
        $name = explode( ' ', $name );
        $this->attributes['customer']['first_name'] = array_shift( $name );
        $this->attributes['customer']['last_name'] = implode( ' ', $name );
    }


    public function setCustomerEmail( $email )
    {
        $this->attributes['customer']['email'] = $email;
    }


    public function setCustomerPhone( $country_code, $phone )
    {
        $this->attributes['customer']['phone'] = [
            'country_code' => $country_code,
            'number'       => $phone,
        ];
    }


    public function setRedirectUrl( $url )
    {
        $this->attributes['redirect']['url'] = $url;
    }


    public function setPostUrl( $url )
    {
        $this->attributes['post']['url'] = $url;
    }


    public function setSource( $source )
    {
        $this->attributes['source']['id'] = $source;
    }


    public function setMetaData( array $meta )
    {
        $this->attributes['metadata'] = $meta;
    }


    public function setRawAttributes( array $attributes )
    {
        $this->attributes = $attributes;
    }


    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
     */
    public function find()
    {
        $this->setMethod( 'get' );
        if (
        $this->validateAttributes(
            [
                'id' => 'required'
            ]
        )
        )
            return $this->send();
    }


    public function refund_response(){
        return $this->send();
    }


    protected function setMethod( $method )
    {
        $this->method = $method;
    }


    /**
     * @param $rules
     *
     * @return bool
     * @throws \Exception
     */
    public function validateAttributes( array $rules, $messages = [] )
    {
        $validator = Validator::make( $this->attributes, $rules, $messages );

        if ( $validator->fails() )
            throw new \Exception( $validator->errors()->first() );

        return true;
    }


    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
     */
    protected function send()
    {
        try {
            $response = $this->client->request(
                $this->method,
                $this->getPath(),
                [
                    'form_params' => $this->attributes,
                    'headers'     => [
                        'Authorization' => 'Bearer ' . $this->token,
                        'Accept'        => 'application/json',
                    ]
                ]
            );

            return new Invoice( json_decode( $response->getBody()->getContents(), true ) );
        }
        catch ( \Throwable $exception ) {
            throw new \Exception( $exception->getMessage() );
        }
    }


    /**
     * @return Invoice
     * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
     */
    public function pay()
    {
//        $rules = [
//            'id'           => 'regex:/^$/i',
//            'amount'       => 'required',
//            'currency'     => 'required',
//            'source.id'    => 'required',
//            'redirect.url' => 'required',
//        ];
//        foreach ( config( 'tap-payment.customer.requirements' ) as $item ) {
//            if ( $item == 'mobile' ) {
//                $rules['customer.phone'] = 'required';
//                $rules['customer.phone.country_code'] = [ 'required', 'numeric' ];
//                $rules['customer.phone.number'] = [ 'required', 'numeric' ];
//            } else {
//                $rules[ 'customer.' . $item ] = 'required';
//            }
//        }

//        if ($this->validateAttributes($rules, [
//                'id.regex' => "ID should be empty when you create a new Charge."
//            ]))
            return $this->send();
    }
}
