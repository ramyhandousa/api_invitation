<?php


namespace App\Libraries\payment_tap;


class Refund extends AbstractService
{
    protected $endpoint = 'refunds/';
    protected $method = 'post';
    protected $attributes = [];
    protected $token;

    public function __construct()
    {
        $this->token = 'sk_live_SxW7el2OTJ90DCkg3KcqawVn';
//        $this->token = 'sk_test_IuhcaB1sO9TibDfjzGrYwLqC';

        parent::__construct();
    }


    protected function setEndpoint( $endpoint )
    {
        $this->endpoint .= $endpoint;
    }


    public function setAmount( $amount )
    {
        $this->attributes['amount'] = $amount;
    }

    public function setChargeId( $charge_id )
    {
        $this->attributes['charge_id'] = $charge_id;
    }

    public function setReason( $reason )
    {
        $this->attributes['reason'] = $reason;
    }


    public function setCurrency( $currency )
    {
        $this->attributes['currency'] = $currency;
    }

    public function setLimit( $limit )
    {
        $this->attributes['limit'] = $limit;
    }

    public function refund_response(){

        return $this->send();
    }

    public function refund_list(){

         $this->setEndpoint('list');

        return $this->send();
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
     */
    protected function send()
    {
        try {
            $response = $this->client->request(
                $this->method,
                $this->getPath(),
                [
                    'form_params' => $this->attributes,
                    'headers'     => [
                        'Authorization' => 'Bearer ' . $this->token,
                        'Accept'        => 'application/json',
                    ]
                ]
            );

            return new Invoice( json_decode( $response->getBody()->getContents(), true ) );
        }
        catch ( \Throwable $exception ) {
            throw new \Exception( $exception->getMessage() );
        }
    }


}
