<?php
/**
 * Created by PhpStorm.
 * User: Hassan Saeed
 * Date: 2/22/2018
 * Time: 4:17 PM
 */

namespace App\Libraries\FirebasePushNotifications;


use Carbon\Carbon;

class Push
{
    private $id;
    private $userId;
    private $userImage;
    private $conversationId;
    private $fileName;
    private $url;
    private $orderId;
    private $orderType;
    private $productId;
    private $type;
    private $is_read;
    private $title;
    private $message;
    private $image;
    private $data;
    private $is_background;
    private $created_at;


    function __construct()
    {

    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function setUserImage($userImage)
    {
        $this->userImage = $userImage;
    }
    public function setConversationId($conversationId)
    {
        $this->conversationId = $conversationId;
    }
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }
    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setIsRead($read)
    {
        $this->is_read = $read;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setImage($imageUrl)
    {
        $this->image = $imageUrl;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setIsBackground($is_background)
    {
        $this->is_background = $is_background;
    }
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    public function getPushData()
    {
        $res = array();
        $res['id']                   = $this->id;
        $res['user_id']              = $this->userId;
        $res['user_image']           = $this->userImage;
        $res['conversation_id']      = $this->conversationId;
        $res['message']              = $this->message;
        $res['fileName']             = $this->fileName;
        $res['url']                  = $this->url;
        $res['order_id']             = $this->orderId;
        $res['order_type']           = $this->orderType;
        $res['product_id']           = $this->productId;
        $res['type']                 = $this->type;
        $res['is_read']              = $this->is_read;
        $res['title']                = $this->title;
        $res['body']                 = $this->message;
        $res['created_at']           = Carbon::parse($this->created_at)->format('Y-m-d H:i');
        $res['timestamp']            = date('Y-m-d G:i:s');
        return $res;
    }


    public function getPushNotification()
    {

        $res = array();
        $res['title']       = $this->title;
        $res['body']        = $this->message;
        $res['icon']        = "ic_launcher";
        $res['sound']        = "default";
        $res['additional']  = $this->data;
        $res['timestamp']   = date('Y-m-d G:i:s');

        return $res;
    }

}
