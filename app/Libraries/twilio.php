<?php


namespace App\Libraries;


use Twilio\Rest\Client;

class twilio
{


    public static function sendMessage($message, $phone_number)
    {

        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv("TWILIO_NUMBER");

        $client = new Client($account_sid, $auth_token);
        $client->messages->create($phone_number,
            ['from' => $twilio_number, 'body' => $message] );

        return $client;
    }


}
