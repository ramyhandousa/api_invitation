<?php

namespace App\Listeners;

use App\Events\AcceptedOrRefuseInvitation;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Device;
use App\Models\Notification;
use App\Models\NotificationInvitation;
use Illuminate\Support\Facades\Log;

class AcceptedOrRefuseInvitationListener
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  AcceptedOrRefuseInvitation  $event
     * @return void
     */
    public function handle(AcceptedOrRefuseInvitation $event)
    {
       $status_number = $event->status == true ? 4 : 5;

       $accOrRefuse =  $event->status == true ? ' قبول ' : 'رفض ' ;
       $title =  'طلبات الدعوات' ;
       $body =    $event->user->name .' تم '. $accOrRefuse . ' طلب الدعوة من '  ;

        $devices = Device::where('user_id', $event->invitation->user_id )->pluck('device');

        $notification = Notification::where('user_id' ,$event->invitation->user_id)->where('invitation_id',$event->invitation->id)->first();

        if ($notification){

            NotificationInvitation::create(['notification_id' => $notification->id,'user_id' => $event->user->id ,'status' =>  -1]);

            if(count($devices ) > 0  ) {

                $this->push->sendPushNotification($devices, null, $title, $body,
                    [
                        'title'         => $title,
                        'orderId'       => $event->invitation->id,
                        'body'          => $body,
                        'type'          => $status_number,
                    ]
                );
            }
        }


    }
}
