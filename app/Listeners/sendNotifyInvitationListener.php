<?php

namespace App\Listeners;

use App\Events\sendNotifyInvitation;
use App\Libraries\PushNotification;
use Illuminate\Support\Facades\Log;

class sendNotifyInvitationListener
{
    public $pushNotification;
    public function __construct(PushNotification  $pushNotification)
    {
       $this->pushNotification  = $pushNotification;
    }

    /**
     * Handle the event.
     *
     * @param  sendNotifyInvitation  $event
     * @return void
     */
    public function handle(sendNotifyInvitation $event)
    {
        $nameInvitation = $event->invitation->name;

        $addressInvitation = $event->invitation->city->name_ar . ' , ' . $event->invitation->address;

        if ($event->status == 'store'){

            $title_invitation = 'دعوة جديدة' ;

        }elseif ($event->status == 'edit'){

            $title_invitation =  '  دعوة ' . $nameInvitation ;

        }else{
            $title_invitation  = $event->status;
        }

        $this->pushNotification->sendPushNotification($event->devices,null,$title_invitation,$addressInvitation,[

                'orderId' => $event->invitation->id,
                'title'         => $nameInvitation,
                'body'          =>  $addressInvitation,
                'type'          => 3
        ]);
    }
}
