<?php

namespace App\Listeners;

use App\Events\ScanInvitation;
use App\Libraries\PushNotification;
use App\Models\Device;
use Illuminate\Support\Facades\Log;

class ScanInvitationListener
{
    public $push;
    public function __construct(PushNotification  $pushNotification)
    {
        $this->push  = $pushNotification;
    }


    public function handle(ScanInvitation $event)
    {
        $devices = Device::where('user_id', $event->user->id )->pluck('device');
        $title = 'بوابة الدخول';
        $body = $event->status == true ? ' تم تسجيل دخول بنجاح' : 'تم تسجيل خروج بنجاح';

        $this->push->sendPushNotification($devices,null,$title,$body,[
            'orderId'   => $event->invitation->id,
            'title'     => $title,
            'body'      =>  $body,
        ]);
    }
}
