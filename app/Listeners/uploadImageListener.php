<?php

namespace App\Listeners;

use App\Events\uploadImage;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
class uploadImageListener
{

    public function __construct()
    {

    }


    public function handle(uploadImage $event)
    {
        $model = $event->model;

        $image = $event->request->file('image');

        $name = time().'.'.$image->getClientOriginalExtension();

        $image->storeAs('images' ,$name);

        if ($model->image){
            unlink($this->public_path($model->image));
        }
        $model->update(['image' => 'images/'.$name]);
    }

    function public_path($path = null)
    {
        return rtrim('/home/da3waapp/public_html/public/' . $path, '/');
    }
}
