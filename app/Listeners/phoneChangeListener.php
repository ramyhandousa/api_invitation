<?php

namespace App\Listeners;

use App\Events\phoneChange;
use App\Http\Helpers\Sms;
use App\Models\VerifyUser;

class phoneChangeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  phoneChange  $event
     * @return void
     */
    public function handle(phoneChange $event)
    {
        $data = [ 'phone' => $event->request->phone , 'action_code'  => $event->code ];

         VerifyUser::updateOrCreate(['user_id' => $event->user->id], $data);

        Sms::sendMessage(' كود التفعيل الخاص بي تطبيق دعوة ' . $event->code , $event->request->phone );
    }
}
