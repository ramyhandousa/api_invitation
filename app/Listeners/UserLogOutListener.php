<?php

namespace App\Listeners;

use App\Events\UserLogOut;
use App\Models\Device;
use Illuminate\Support\Str;

class UserLogOutListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLogOut  $event
     * @return void
     */
    public function handle(UserLogOut $event)
    {
//        $event->user->update(['api_token' => Str::random(60)]);

        if ($event->request->deviceToken){

            $device = Device::where('user_id', $event->user->id)->where('device', $event->request->deviceToken)->first();

            if ($device) $device->delete();
        }
    }
}
