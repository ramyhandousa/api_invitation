<?php

namespace App\Providers;

use App\Events\AcceptedOrRefuseInvitation;
use App\Events\ContactUs;
use App\Events\phoneChange;
use App\Events\ScanInvitation;
use App\Events\sendNotifyInvitation;
use App\Events\uploadImage;
use App\Events\UserLogOut;
use App\Listeners\AcceptedOrRefuseInvitationListener;
use App\Listeners\ContactUsListener;
use App\Listeners\phoneChangeListener;
use App\Listeners\ScanInvitationListener;
use App\Listeners\sendNotifyInvitationListener;
use App\Listeners\uploadImageListener;
use App\Listeners\UserLogOutListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\ExampleEvent' => [
            'App\Listeners\ExampleListener',
        ],
        phoneChange::class => [
            phoneChangeListener::class
        ],
        uploadImage::class => [
            uploadImageListener::class
        ],
        UserLogOut::class => [
            UserLogOutListener::class
        ],
        ContactUs::class => [
            ContactUsListener::class
        ],
        sendNotifyInvitation::class => [
            sendNotifyInvitationListener::class
        ],
        AcceptedOrRefuseInvitation::class => [
            AcceptedOrRefuseInvitationListener::class
        ],
        ScanInvitation::class =>[
            ScanInvitationListener::class
        ]

    ];
}
