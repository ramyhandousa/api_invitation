<?php

namespace App\Rules;

use App\Models\VerifyUser;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class checkCodeActivation implements Rule
{
    public $phone;
    public function __construct( $phone)
    {
        $this->phone = $phone;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {


        return VerifyUser::where('phone',$this->phone )->where('action_code',$value)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('global.activation_code_not_correct') ;
    }
}
