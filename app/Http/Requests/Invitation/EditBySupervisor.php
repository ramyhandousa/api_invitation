<?php

namespace App\Http\Requests\Invitation;

use Anik\Form\FormRequest;

class EditBySupervisor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'guests.*'                      => 'array',
            'guests.*.id'                   => 'exists:contact_invitations,id',
            'guests.*.phone'                => 'distinct|min:8|max:20',
            'guests.*.guest_status'         => 'in:vip,normal',
            'guests.*.table_number'         => 'numeric',
        ];
    }
}
