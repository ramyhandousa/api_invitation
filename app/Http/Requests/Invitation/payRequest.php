<?php

namespace App\Http\Requests\Invitation;

use Anik\Form\FormRequest;

class payRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
//            'payment' => 'required|in:apple_pay,stc,mada',
            'paymentId' => 'required|numeric',
            'payment_money' => 'required|numeric|min:1'
        ];
    }
}
