<?php

namespace App\Http\Requests\Invitation;

use Anik\Form\FormRequest;

class scanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'phone' => 'required|exists:contact_invitations,phone'
        ];
    }
}
