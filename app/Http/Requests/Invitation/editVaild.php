<?php

namespace App\Http\Requests\Invitation;

use Anik\Form\FormRequest;

class editVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'name'          => 'min:5|max:50',
            'date'          => 'date|date_format:Y-m-d|after:yesterday',
            'time_from'     => 'date_format:H:i:s',
            'time_to'       => 'date_format:H:i:s',
            'number_guests' => 'numeric|min:1|max:10000',
            'city_id'       => 'exists:cities,id',
            'desc'          => 'min:1|max:10000',
            'latitude'      => 'min:1|max:10000',
            'longitude'     => 'min:1|max:10000',
            'address'       => 'min:1|max:10000',
            'image'                         => 'image|mimes:jpeg,png,jpg|max:10240',
            'guests.*'                      => 'array',
            'guests.*.phone'                => 'distinct|min:8|max:20',
            'guests.*.guest_status'         => 'in:vip,normal',
            'guests.*.table_number'         => 'numeric',
            'supervisor.*'                  => 'array',
            'supervisor.*.guest_login'      => 'boolean',
            'supervisor.*.add_invitees'     => 'boolean',
            'supervisor.*.edit_invitation'  => 'boolean',
        ];
    }


}
