<?php

namespace App\Http\Requests\Payment;

use Anik\Form\FormRequest;

class PayOnline extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'invitation_id'     => 'required|exists:invitations,id',
            'payment_money'     => 'required|numeric|min:1',
            'addition_number'   => 'required|numeric',
        ];
    }
}
