<?php

namespace App\Http\Requests\setting;

use Anik\Form\FormRequest;

class contactVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'type'    => 'required|exists:type_supports,id',
            'message'    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'برجاء اختيار قسم التواصل',
            'message.required' => 'مطلوب الرسالة الخاصه بك',
        ];
    }
}
