<?php

namespace App\Http\Requests\setting;

use Anik\Form\FormRequest;

class bankTransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'image'     => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'bankId'    => 'required|exists:banks,id',
            'price'     => 'required|numeric|min:1|max:100000',
            'addition_number' => 'required'
        ];
    }


}
