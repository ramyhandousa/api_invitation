<?php

namespace App\Http\Requests\order;

use Anik\Form\FormRequest;

class orderRefuse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'message' => 'required'
        ];
    }
}
