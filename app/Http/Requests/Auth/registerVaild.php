<?php

namespace App\Http\Requests\Auth;

use Anik\Form\FormRequest;

class registerVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'name' => 'required|max:50',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users|numeric',
            'password' => 'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => trans('validation.required'),
            'email.required' => trans('validation.required'),
            'email.unique' => trans('global.unique_email'),
            'phone.unique' => trans('global.unique_phone'),
            'phone.required' => trans('validation.required'),
            'password.required' => trans('validation.required'),
        ];
    }
}
