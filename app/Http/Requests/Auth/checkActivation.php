<?php

namespace App\Http\Requests\Auth;

use Anik\Form\FormRequest;
use App\Models\VerifyUser;
use App\Rules\checkCodeActivation;

class checkActivation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'phone' => 'required|exists:verify_users,phone',
            'code'        => ['required', 'exists:verify_users,action_code' , new checkCodeActivation($this->phone)],
        ];
    }


    public function messages()
    {
        return [
            'phone.required' => trans('global.required'),
            'phone.exists' => trans('global.phone_number_notFound'),
            'code.required' => 'إدخل الكود من فضلك',
            'code.exists' =>  trans('global.activation_code_not_correct'),
        ];
    }


}
