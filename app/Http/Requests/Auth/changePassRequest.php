<?php

namespace App\Http\Requests\Auth;

use Anik\Form\FormRequest;
use App\Rules\checkOldPass;

class changePassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'oldPassword' => [   new checkOldPass() ]
        ];
    }


}
