<?php

namespace App\Http\Requests\Auth;

use Anik\Form\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;


class editUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();
        return [
            'name'          => 'max:255',
            'phone'         => 'numeric|digits_between:9,15|unique:users,phone,' . $user->id ,
            'email'         => 'email|unique:users,email,' . $user->id ,
            'image'         => 'image|mimes:jpeg,png,jpg|max:10240'
        ];
    }


    public function messages()
    {
        return [
            'email.unique' => trans('global.unique_email'),
            'email.email' => trans('validation.email'),
            'phone.unique' => trans('global.unique_phone'),
        ];
    }

}
