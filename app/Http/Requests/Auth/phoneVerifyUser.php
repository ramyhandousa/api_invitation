<?php

namespace App\Http\Requests\Auth;

use Anik\Form\FormRequest;

class phoneVerifyUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|exists:verify_users,phone',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => trans('global.required'),
            'phone.exists' => trans('global.phone_number_notFound'),
        ];
    }
}
