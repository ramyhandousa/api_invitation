<?php

namespace App\Http\Requests\Auth;

use Anik\Form\FormRequest;
use Illuminate\Validation\Validator;

class loginVaild extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'phone' => 'required|exists:users,phone',
            'password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => trans('global.required'),
            'phone.exists' => trans('global.phone_number_notFound'),
            'password.required' => trans('global.required'),
        ];
    }


}
