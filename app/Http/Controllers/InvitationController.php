<?php

namespace App\Http\Controllers;

use App\Events\ScanInvitation;
use App\Events\uploadImage;
use App\Http\Requests\Invitation\EditBySupervisor;
use App\Http\Requests\Invitation\editVaild;
use App\Http\Requests\Invitation\payRequest;
use App\Http\Requests\Invitation\scanRequest;
use App\Http\Requests\Invitation\storeVaild;
use App\Http\Resources\invitation\filterRelation;
use App\Http\Resources\invitation\showGuestResource;
use App\Http\Resources\invitation\showResource;
use App\Http\Resources\invitation\showSupervisorResource;
use App\Jobs\SendSmsQueue;
use App\Models\ContactInvitation;
use App\Models\Invitation;
use App\Models\PermissionContact;
use App\Models\Setting;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InvitationController extends Controller
{

    use RespondsWithHttpStatus;
    public  $lang;
    public function __construct(Request  $request)
    {
        app('translator')->setLocale($request->headers->get('Accept-Language') ?  : 'ar');

        $this->lang = app('translator')->getLocale();

        $this->middleware('auth');
    }


    public function user(scanRequest  $request, $id){

        $invitation  = ContactInvitation::wherePhone($request->phone)->where('invitation_id',$id)->with('invitation')->firstOrFail();

        if ( $invitation['type_user'] == 'supervisor'){

            return $this->success( ' بيانات  الدعوة الخاصه   ', new showSupervisorResource($invitation) );
        }else{

            return $this->success(  ' بيانات  الدعوة الخاصه   ', new showGuestResource($invitation) );
        }

    }


    public function show($id){

        $invitation =  Invitation::whereHas('contact')->with('contact','guest_contact','supervisor_contact')->findOrFail($id);

        return $this->success('تفاصيل الدعوة' , new showResource($invitation));
    }

    public function show_details($id, Request  $request){
        $invitation =  Invitation::whereHas('contact')->with('guest_contact','supervisor_contact')->findOrFail($id);

        if (isset($request->supervisor)){

            $contact = $invitation['supervisor_contact'];

            $message = 'المشرفين ';
        }else{

            $contact = $invitation['guest_contact'];
            $message = 'المدعوين ';
        }

        return $this->success($message , filterRelation::collection($contact));
    }

    public function store(storeVaild $request)
    {
        $data = collect($request->validated())->except(['image','guests', 'supervisor'])->toArray();

//        $allPhones =   collect($request->guests)->pluck('phone')->merge(collect( $request->supervisor)->pluck('phone'));

        $invitation = Invitation::create($data);

        if (is_file($request->image)){
            \event(new uploadImage($invitation,$request));
        }
         $this->storePermission($request->guests, $invitation);

        $this->storePermission($request->supervisor, $invitation,'supervisor');

//        $this->dispatch(new SendSmsQueue($invitation,$allPhones,'store'));

        return $this->success('تم إنشاء الدعوة' , new showResource($invitation));
    }

    public function editInvitation(editVaild $request, $id ){

        $invitation = Invitation::whereHas('contact')->with('guest_contact','supervisor_contact')->findOrfail($id);

        $this->authorize('update_invitationPolicy',$invitation);

        $data = collect($request->validated())->except(['number_guests','image','guests', 'supervisor'])->toArray();
        $invitation->fill($data);
        $invitation->save();

        $this->updateNumberGuesses($request, $invitation);

        if (is_file($request->image)){
            \event(new uploadImage($invitation,$request));
        }

        if ( ($request->guests   && count($request->guests) > 0 || $request->supervisor && count($request->supervisor) > 0 )){

            if ($request->guests && count($request->guests) > 0){
                $invitation->guest_contact->each->delete();
                $this->edit_invitation_guests($request,$invitation);
            }

            if ($request->supervisor && count($request->supervisor) > 0){
                $invitation->supervisor_contact->each->delete();
                $this->edit_invitation_supervisor($request,$invitation);
            }

            $contact_invitation = ContactInvitation::select('id','phone','is_payed')->where('invitation_id',$invitation->id)->whereIsPayed(0)->get();

            $allPhones = $contact_invitation->pluck('phone');

            $this->dispatch(new SendSmsQueue($invitation,$allPhones,'edit'));

            $contact_invitation->each->update(['is_payed' => 1]);
        }

        return $this->success('تم تغير بيانات الدعوة بنجاح' , new showResource($invitation));
    }

    public function edit_invitation_by_supervisor(EditBySupervisor  $request , $id){
        $invitation = Invitation::whereHas('guest_contact')->with('guest_contact')->findOrfail($id);

        $this->authorize('update_invitationBySupervisor',$invitation);

        $this->edit_invitation_guests($request, $invitation);

        return $this->success('تم تغير بيانات  المدعوين بنجاح' , new showResource($invitation));
    }

    private function edit_invitation_guests($request, $invitation){

        $guest = $this->storePermission($request->guests, $invitation);

        $invitation->load('guest_contact');
    }

    private function edit_invitation_supervisor($request,$invitation){

        $supervisor = $this->storePermission($request->supervisor, $invitation,'supervisor');

        $invitation->load('supervisor_contact');
    }

    public function updateNumberGuesses($request ,$invitation ){
        if ($request->number_guests < $invitation->number_guests ){

            if ($invitation->is_payed == 0){
                $payment = Setting::getBody('payment_value') *   $request->number_guests  ;
                $invitation->update(['payment_money' => $payment]);
            }

            if ($request->number_guests > $invitation->number_guests){

                $invitation->update(['number_guests' => $request->number_guests]);
            }
        }

        if ($request->number_guests > $invitation->number_guests){

            if ($invitation->is_payed == 0){

                $old_payment = $invitation->payment_money;
                $payment = Setting::getBody('payment_value') *  ($request->number_guests -$invitation->number_guests ) ;
                $invitation->update(['number_guests' => $request->number_guests , 'payment_money' => $old_payment + $payment]);

            }else{

                $payment = Setting::getBody('payment_value') *  ($request->number_guests -$invitation->number_guests ) ;
                $invitation->update(['payment_money' => $payment]);

            }

        }
    }


    public function storePermission($permission , $invitation, $type =null){

        foreach ($permission as $key){

            $data = [
                'type_user'                 => $type  ? $type : 'guest' ,
                'invitation_id'             => $invitation->id,
//                'image'                     => isset($key['image'])                     ? $key['image']          : null,
                'name'                      => isset($key['name']) ? $key['name'] : null,
                'phone'                     => $key['phone'],
                'guest_status'              => isset($key['guest_status'])              ? $key['guest_status']          : "normal",
                'table_number'              => isset($key['table_number'])              ? $key['table_number']          : null,
                'additional_information'    => isset($key['additional_information'])    ? $key['additional_information']  : null ,
                'guest_login'               => isset($key['guest_login'])               ? $key['guest_login']           : false,
                'add_invitees'              => isset($key['add_invitees'])              ? $key['add_invitees']          : false,
                'edit_invitation'           => isset($key['edit_invitation'])           ? $key['edit_invitation']       : false,
            ];

            if (isset($key['id'])){
                $contactInvitation = ContactInvitation::find($key['id']);

                if ($contactInvitation){
                    $contactInvitation->update($data);
                }

            }else{

                ContactInvitation::create($data);
            }

            if (isset($key['phone'])){
                $user = User::wherePhone($key['phone'])->first();
                if ($user){
                    ContactInvitation::where('phone',$key['phone'])->update(['user_id' => $user->id]);
                }
            }

        }
    }


    public function scan_invitation(scanRequest  $request , $id){

        $invitation = Invitation::with('supervisor_contact','guest_contact')->findOrFail($id);

        $supervisor = Auth::user();

        $guest = $request->phone;

        $supervisor_user = $invitation['supervisor_contact']->where('phone', $supervisor->phone)->count() ;

        if ($supervisor_user == 0 && $invitation->user_id != $supervisor->id){
            return $this->failure('يجب ان التأكد من أنك مشرف وصاحب لتلك الدعوة');
        }

        $guest_user = $invitation['guest_contact']->where('phone', $guest)->first() ;

        if (!$guest_user){
            return $this->failure(trans('global.guest_not_found'));
        }

        $user = User::wherePhone($request->phone)->first();

        if ($guest_user->check_in == 0 ){
            event(new ScanInvitation($user, $invitation, true));
            $guest_user->update(['check_in' => 1]);
            $message =  trans('global.guest_login');
        }else{
            event(new ScanInvitation($user, $invitation, false));
            $guest_user->update(['check_in' => 0]);
            $message =  trans('global.guest_logout');
        }

        return $this->success($message , new filterRelation($guest_user));
    }

    public function pay_invitation(payRequest  $request , $id){

        $invitation = Invitation::findOrFail($id);

            $this->authorize('bank_transfer',$invitation);

            if ($invitation->is_payed == 1){
                return $this->failure(' بيانات هذه الدعوة مدفوعة مسبقا..  ');
            }

        $setting_payment = Setting::getBody('payment_value');

        $payment_value = $setting_payment != null ?  $invitation->number_guests * $setting_payment : 0 ;

            if ( $request->payment_money !=  $payment_value){
                return $this->failure( $payment_value .' يجب أن تكون القيمة المدفوعة .. ');
            }

        $invitation->update(array_merge($request->validated() , ['is_payed' => 1]));

      return  $this->success(trans('global.payment_online'));

    }


}
