<?php

namespace App\Http\Controllers;

use App\Events\Event;
use App\Events\phoneChange;
use App\Events\uploadImage;
use App\Events\UserLogOut;
use App\Http\Helpers\Sms;
use App\Http\Requests\Auth\changePassRequest;
use App\Http\Requests\Auth\checkActivation;
use App\Http\Requests\Auth\editUser;
use App\Http\Requests\Auth\phoneVaild;
use App\Http\Requests\Auth\loginVaild;
use App\Http\Requests\Auth\phoneVerifyUser;
use App\Http\Requests\Auth\registerVaild;
use App\Http\Resources\UserEditResource;
use App\Http\Resources\UserResource;
use App\Models\ContactInvitation;
use App\Models\Device;
use App\Models\VerifyUser;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    use RespondsWithHttpStatus;
    public  $path;
    public  $lang;
    public function __construct(Request  $request)
    {
        app('translator')->setLocale($request->headers->get('Accept-Language') ?  : 'ar');

        $this->lang = app('translator')->getLocale();

        $this->path = 'files/';

        $this->middleware('auth', ['only' => [
            'changPassword', 'editProfile', 'logOut'
        ]]);
    }

    public function register(registerVaild $request){

        $data = array_merge($request->validated(),['api_token' => Str::random(60)]);

        $user = User::create($data);

        $action_code = substr(rand(), 0, 4);

        $this->createVerfiy($request, $user, $action_code);

        $this->manageDevices($request, $user);

        Sms::sendMessage(' كود التفعيل الخاص بي تطبيق دعوة ' . $action_code , $request->phone);
        $response = ['code' => $action_code, 'api_token' => $user->api_token];

        return $this->success( trans('global.activation_code_sent'), $response);
    }

    public function login(loginVaild $request){

        $user = User::where('phone',$request->phone)->first();

        if ( !Hash::check($request->password , $user->password)) {

            return  $this->failure(trans('global.username_password_notcorrect'));
        }

        if ( $user->is_suspend  == 1) {
            return  $this->failure($user->message);
        }

        $this->manageDevices($request, $user);

        ContactInvitation::where('phone',$request->phone)->update(['user_id' => $user->id]);

        return $this->success( trans('global.logged_in_successfully'),new UserResource($user));
    }


    public function forgetPassword(phoneVaild $request){

        $user = User::where('phone', $request->phone)->first();

        $action_code = substr(rand(), 0, 4);

        \event(new phoneChange($user,$action_code,$request));

        return $this->success( trans('global.activation_code_sent'),['code' => $action_code]);
    }

    public function resetPassword(phoneVaild $request)
    {
        $user = User::where('phone', $request->phone)->first();

        if ($request->password){

            $user->update(['password' => $request->password]);

            $message = trans('global.password_was_edited_successfully');
        }else{

           $message = trans('global.password_not_edited');
        }
        return $this->success( $message,new UserResource($user));
    }

    public function checkCodeActivation(checkActivation $request)
    {
        $verifyUser = VerifyUser::where('phone', $request->phone)->with('user')->first();

        $user = $verifyUser['user'];

        $phone = $verifyUser->phone ? : $user->phone;

        $user->update(['phone' => $phone ,  'is_active' => 1]);

        $verifyUser->delete();

        $this->manageDevices($request, $user);

        ContactInvitation::where('phone',$request->phone)->update(['user_id' => $user->id]);

        return $this->success( trans('global.your_account_was_activated'),new UserResource($user));

    }

    public function resendCode(phoneVerifyUser $request)
    {
        $verifyUser = VerifyUser::where('phone', $request->phone)->with('user')->first();

        $user = $verifyUser['user'];

        $action_code = substr(rand(), 0, 4);

        \event(new phoneChange($user,$action_code,$request));

        return $this->success( trans('global.activation_code_sent'),['code' => $action_code]);
    }

    public function changPassword(changePassRequest $request)
    {
        $user = Auth::user();

        if ($request->newPassword){

            $user->update( [ 'password' => $request->newPassword] );

            $message =  trans('global.password_was_edited_successfully') ;

        }else{

            $message = trans('global.password_not_edited');
        }

        return $this->success( $message);
    }


    public function editProfile (editUser $request )
    {
        $user = Auth::user();

        $filterData =  collect($request->validated())->except('phone','image');

        $user->fill($filterData->toArray());

        $user->save();

        if ($user->phone != $request->phone){
            $action_code = substr(rand(), 0, 4);
            \event(new phoneChange($user,$action_code,$request));
        }

        if (is_file($request->image)){
            \event(new uploadImage($user,$request));
        }

        $message =  $user->wasChanged() ? trans('global.profile_edit_success') :trans('global.profile_not_edit_success');

        return $this->success($message , new UserEditResource($user));
    }

    public function logOut(Request  $request)
    {
        $user = Auth::user();

        event(new UserLogOut($user,$request));

        return $this->success(trans('global.logged_out_successfully'));
    }

    public function createVerfiy($request , $user , $action_code){
        $verifyPhone = new VerifyUser();
        $verifyPhone->user_id       =    $user->id;
        $verifyPhone->phone         =    $request->phone;
        $verifyPhone->action_code   =    $action_code;
        $verifyPhone->save();
    }

    public function manageDevices($request, $user)
    {
        if ($request->deviceToken) {

            $device = Device::where('device', $request->deviceToken)->first();

            if (!$device) {
                $data = new Device();
                $data->device = $request->deviceToken;
                $data->user_id = $user->id;
                $data->device_type = $request->deviceType;
                $data->save();
            }else{
                $device->update(['user_id' => $user->id , 'device' => $request->deviceToken ]);
            }
        }
    }
}
