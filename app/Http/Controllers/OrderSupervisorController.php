<?php

namespace App\Http\Controllers;

use App\Http\Resources\Orders\OrderListResource;
use App\Models\Invitation;
use App\Traits\RespondsWithHttpStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderSupervisorController extends Controller
{
    use RespondsWithHttpStatus;
    public  $lang;
    public function __construct(Request  $request)
    {
        app('translator')->setLocale($request->headers->get('Accept-Language') ?  : 'ar');

        $this->lang = app('translator')->getLocale();

        $this->middleware('auth');

    }

    public function list(Request  $request){

        $user = $request->user();

        $query = Invitation::whereUserId($user->id);

        if (isset($request->old)){
            $query->whereDate('date' ,'<', date("Y-m-d"));
        }else{

            $query->whereDate('date' ,'>=' , date("Y-m-d"));
        }

        $query->orWhereHas('supervisor_contact',function ($contact) use ($user){
            $contact->where('phone',$user->phone);
        });

        $count = $query->count();

        $this->pagination_query($request,$query);

        $list =  OrderListResource::collection($query->get());

        return $this->successWithPagination('واجهة الداعي',$count,$list);
    }




    public function pagination_query($request ,$query ){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1
        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
        return $query->latest();
    }
}
