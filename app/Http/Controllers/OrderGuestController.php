<?php

namespace App\Http\Controllers;

use App\Events\AcceptedOrRefuseInvitation;
use App\Http\Requests\order\orderRefuse;
use App\Http\Resources\Orders\AcceptedOrRefuseResource;
use App\Http\Resources\Orders\OrderListGuestResource;
use App\Models\ContactInvitation;
use App\Models\NotificationInvitation;
use App\Traits\RespondsWithHttpStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderGuestController extends Controller
{
    use RespondsWithHttpStatus;
    public  $lang;
    public function __construct(Request  $request)
    {
        app('translator')->setLocale($request->headers->get('Accept-Language') ?  : 'ar');

        $this->lang = app('translator')->getLocale();

        $this->middleware('auth');

    }

    public function list(Request  $request){


        $user = Auth::user();

        $query = ContactInvitation::whereHas('invitation',function ($invitation){
                    $invitation->where('is_payed' , 1);
            })
            ->where('type_user','=','guest')->where('phone',$user->phone)
            ->select('id','invitation_id','created_at','status');

        if (isset($request->old)){
            $query->whereHas('invitation',function ($q){
                $q->whereDate('date' ,'<', Carbon::today());
            });
        }else{
            $query->whereHas('invitation',function ($q){
                $q->whereDate('date','>=' , Carbon::today());
            });

        }
        $count = $query->count();

        $this->pagination_query($request,$query);

        $list =  OrderListGuestResource::collection($query->get());

        return $this->successWithPagination('واجهة الضيف',$count,$list);
    }


    public function accepted($id){

        $user = Auth::user();

        $invitation  = ContactInvitation::wherePhone($user->phone)->where('invitation_id',$id)->with('invitation')->firstOrFail();

        $status = $invitation['status'] ;

        if ($status == 'pending'){

            NotificationInvitation::where('user_id' , $user->id)
                ->whereHas('notification',function ($notification) use ($id){
                $notification->where('invitation_id' , $id);
            })->update(['status' => 1]);

            event(new AcceptedOrRefuseInvitation($user,$invitation['invitation'],true));

            $invitation->update(['status' => 'accepted']);

            return $this->success('تم قبول الدعوة ', new AcceptedOrRefuseResource($invitation) );
        }else{

           return $this->failure('لأسف تم قبول او رفض الدعوة مسبقا');
        }
    }


    public function refuse(orderRefuse  $request , $id){

        $user = Auth::user();

        $invitation  = ContactInvitation::wherePhone($user->phone)->where('invitation_id',$id)->with('invitation')->firstOrFail();

        $status = $invitation['status'] ;

        if ($status == 'pending'){

            NotificationInvitation::where('user_id' , $user->id)
                ->whereHas('notification',function ($notification) use ($id){
                    $notification->where('invitation_id' , $id);
                })->update(['status' => -1]);

            event(new AcceptedOrRefuseInvitation($user,$invitation['invitation'],false));

            $invitation->update(['status' => 'refuse' , 'message' => $request->message]);

            return $this->success('تم رفض  الدعوة ', new AcceptedOrRefuseResource($invitation) );

        }else{

            return $this->failure('لأسف تم قبول او رفض الدعوة مسبقا');
        }
    }

    public function pagination_query($request ,$query ){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1
        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
        return $query->latest();
    }
}
