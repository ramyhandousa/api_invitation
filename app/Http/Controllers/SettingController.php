<?php

namespace App\Http\Controllers;

use App\Events\ContactUs;
use App\Events\uploadImage;
use App\Http\Requests\setting\bankTransferRequest;
use App\Http\Requests\setting\contactVaild;
use App\Http\Resources\filterIdAndNameResource;
use App\Http\Resources\lists\BankResource;
use App\Http\Resources\lists\CityResource;
use App\Http\Resources\lists\TypeSupportResource;
use App\Models\Bank;
use App\Models\BankTransfer;
use App\Models\City;
use App\Models\Invitation;
use App\Models\Payment;
use App\Models\Setting;
use App\Models\Support;
use App\Models\TypeSupport;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{

    use RespondsWithHttpStatus;
    public  $lang;
    public function __construct(Request  $request)
    {
        app('translator')->setLocale($request->headers->get('Accept-Language') ?  : 'ar');

        $this->lang = app('translator')->getLocale();

        $this->middleware('auth',['only' => ['contact_us' ,'bank_transfer' ] ]);

    }

    public function listCities(){

        $list = City::select('id','name_'. $this->lang)->get();

        $data = CityResource::collection($list);

        return  $this->success('المدن',$data);
    }

    public function listBanks(){

        $list = Bank::select('id','name_'. $this->lang ,'image','number')->whereIsSuspend(0)->get();

        $data = BankResource::collection($list);

        return  $this->success('الحسابات البنكية',$data);
    }

    public function listTypes(){

        $list = TypeSupport::select('id','name_'. $this->lang)->get();

        $data = TypeSupportResource::collection($list);

        return  $this->success('أنواع التواصل  ',$data);
    }

    public function aboutUs(){
        $about_us =  Setting::whereIn('key',['title_about_us_'. $this->lang,'about_us_' . $this->lang])->get();

        $title = $about_us->where('key','title_about_us_'. $this->lang)->first();
        $body = $about_us->where('key','about_us_'. $this->lang)->first();

        $data = [
            'title' =>$title ? $title->body : null,
            'body' =>  $body ? $body->body : null ,
        ];
        return $this->success('عن التطبيق ', [$data]);
    }

    public function terms_user(){

        $setting = Setting::whereIn('key',['title_terms_user_' . $this->lang ,'terms_user_' . $this->lang])->get();

        $title = $setting->where('key','title_terms_user_'. $this->lang)->first();
        $body = $setting->where('key','terms_user_'. $this->lang)->first();

        $data = [
            'title' =>$title ? $title->body : null,
            'body' =>  $body ? $body->body : null ,
        ];
        return $this->success( 'الشروط والأحكام',[ $data]);
    }

    public function social(){

        $setting = Setting::whereIn('key',['faceBook', 'twitter', 'instagram','snapchat'])->get();


        $data = ['facebook' => $setting[0]->body , 'twitter' => $setting[1]->body ,
                    'instagram' => $setting[3]->body, 'snapchat' => $setting[3]->body ];

        return $this->success( 'روابط التواصل الإجتماعي', $data);
    }

    public function contact_us(contactVaild $request ){
        $user = Auth::user();
        $support = new Support();
        $support->user_id = 1;
        $support->sender_id = $user->id;
        $support->type_id = (int) $request->type;
        $support->message = $request->message;
        $support->save();

        event(new ContactUs(1,$user->id,$request));

        return $this->success( trans('global.message_was_sent_successfully'));
    }

    public function bank_transfer(bankTransferRequest $request , $id){
        $invitation = Invitation::findOrFail($id);

        $this->authorize('bank_transfer',$invitation);

        $user = Auth::user();

        $transfer_before = BankTransfer::whereUserId($user->id)->where('invitaion_id' , $invitation->id)->where('is_accepted',0)->first();

        if ($transfer_before){
            return  $this->failure( trans('global.bank_transfers_before'));
        }

        if ($request->price > $invitation->payment_money || $request->price <  $invitation->payment_money){
            return  $this->failure( 'يجب التأكد من القيمة التي سوف يتم دفعها');
        }

        $bankTransfer               = new BankTransfer();
        $bankTransfer->user_id      = $user->id;
        $bankTransfer->invitaion_id = $invitation->id;
        $bankTransfer->bank_id      = $request->bankId;
        $bankTransfer->payment      = $request->price;
        $bankTransfer->addition_number = $request->addition_number;
        $bankTransfer->save();

        if (is_file($request->image)){
            \event(new uploadImage($bankTransfer,$request));
        }

        Payment::create(['user_id' => $user->id,'invitation_id' => $invitation->id ,
                        'bank_transfer_id' => $bankTransfer->id, 'payment' => 'bank_transfer',
                        'price' => $request->price]);

        return $this->success( trans('global.success_progress'));
    }
}
