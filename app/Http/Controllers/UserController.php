<?php

namespace App\Http\Controllers;

use App\Http\Resources\Notification\InvitationNotifactionResource;
use App\Http\Resources\Notification\ListNotificationResource;
use App\Models\Notification;
use App\Models\NotificationInvitation;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    use RespondsWithHttpStatus;
    public  $lang;
    public function __construct(Request  $request)
    {
        app('translator')->setLocale($request->headers->get('Accept-Language') ?  : 'ar');

        $this->lang = app('translator')->getLocale();

        $this->middleware('auth');

    }

    public function list_notification(Request  $request){

        $user = Auth::user();

        $query = Notification::whereUserId($user->id)->orWhereHas('notify_invitation',function ($q) use ($user){
            $q->where('user_id',$user->id);
        });

        $notifications = $query->get();

        $query->get()->each->update(['is_read' => 1]);

        $data =  ListNotificationResource::collection($notifications);

        return $this->success('الإشعارات', $data);
    }

    public function invitation_notifications($id){

        $notifications =  Notification::whereHas('notify_invitation')->with(['skip_invitation_last.user'])->findOrFail($id);

        $notifications->notify_invitation->each->update(['is_read' => 1]);

        $data =  InvitationNotifactionResource::collection($notifications['skip_invitation_last']);

        return $this->success('تفاصيل الإشعار للدعوة', $data);
    }
}
