<?php

namespace App\Http\Controllers;

use App\Http\Requests\Payment\PayOnline;
use App\Jobs\SendSmsQueue;
use App\Libraries\payment_tap\TapService;
use App\Models\ContactInvitation;
use App\Models\Invitation;
use App\Models\Payment;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{

    use RespondsWithHttpStatus;
    public  $lang;
    public function __construct(Request  $request)
    {
        app('translator')->setLocale($request->headers->get('Accept-Language') ?  : 'ar');

        $this->lang = app('translator')->getLocale();

        $this->middleware('auth', ['only' => [
            'pay_online',
        ]]);

    }

    public function pay_online(PayOnline $request){

        $invitation = Invitation::findOrFail($request->invitation_id);

        if ($invitation->payment_money != $request->payment_money){
            return  $this->failure('من فضلك يجب التأكد من المبلغ المراد دفعه');
        }

        $user = Auth::user();

        $payment = new TapService();

        try{

            $payment = $payment->createCharge();

            $payment->setCustomerName( $user->name );

            $payment->setCustomerPhone( "+05",$user->phone );

            $payment->setAmount( $request->payment_money );

            $payment->setCurrency( "SAR" );

            $payment->setSource( "src_all" );

            $payment->setRedirectUrl(  "https://api.da3waapp.com/v1/payment/success?invitationId=$request->invitation_id&addition_number=$request->addition_number" );

            $payment->setPostUrl(  "https://api.da3waapp.com/v1/payment/error" ); // if you are using post request to handle payment updates

            $invoice = $payment->pay();

            $data = ['url' => $invoice->getPaymetUrl()];

            return $this->success('تم الدفع بنجاح', $data);

        } catch( \Exception $exception ){

            return  $this->failure($exception->getMessage());
        }
    }

    public function success_payment(Request  $request ){

        $payment_id = $request->tap_id;

        $invoice = TapService::findCharge( $payment_id );

        if ($invoice->isSuccess()){

            $invitation = Invitation::findOrFail($request->invitationId);

            Payment::create(['user_id'      => $invitation->user_id,
                'invitation_id' => $invitation->id ,
                'payment'       => 'online',
                'price'         => $invitation->payment_money,
                'is_review'     => 1,
                'paymentId'     => $payment_id,
            ]);

            $total_number_guesses = $invitation->number_guests + $request->addition_number;

            $invitation->update(['is_payed' => 1 , 'payment_money' => 0 , 'number_guests' => $total_number_guesses ]);

            $contact_invitation = ContactInvitation::select('id','phone','is_payed')->where('invitation_id',$invitation->id)->whereIsPayed(0)->get();

            $allPhones = $contact_invitation->pluck('phone');

            $this->dispatch(new SendSmsQueue($invitation,$allPhones,'store'));

            $contact_invitation->each->update(['is_payed' => 1]);

            return view('payment');

        }else{

            return view('errorPayment');
        }

    }

    public function error(){

        return view('errorPayment');
    }


}
