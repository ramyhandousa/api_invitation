<?php

namespace App\Http\Resources\invitation;

use App\Http\Resources\lists\CityResource;
use App\Models\BankTransfer;
use App\Models\Setting;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class showResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $user = Auth::user();
        $contact = $this->contact->where('phone',$user->phone )->first();

        return [
            'id'                => $this->id,
            'host_name'         => $this->user->name,
            'name'              => $this->name,
            'image'             => $this->when( $this->image, env('APP_URL').'/'.$this->image),
            'date'              => $this->date,
            'time_from'         => $this->time_from,
            'time_to'           => $this->time_to,
            'number_guests'     => $this->number_guests,
            'desc'              => $this->desc,
            'latitude'          => $this->latitude,
            'longitude'         => $this->longitude,
            'address'           => $this->address,
            'city'              =>  new CityResource($this->city) ,
            'is_payed'          => (boolean) $this->is_payed,
            'has_transfer'      => BankTransfer::whereUserId( $this->user->id)->where('invitaion_id' , $this->id)->where('is_accepted',0)->exists(),
//            'payment_system'    => Setting::getBody('payment_value') * $this->number_guests,
            'payment_money'     => $this->payment_money,
            'count_guest'       => $this->guest_contact->count() > 7,
            'count_supervisor'  => $this->supervisor_contact->count() > 2,
            $this->mergeWhen($contact , [
                'info_user'         => $contact ? $contact->type_user : null,
               'user_enter' => new filterUserEnter($contact)
            ]),
            'guest'             => filterRelation::collection($this->guest_contact->take(7)),
            'supervisor'        => filterRelation::collection($this->supervisor_contact->take(2)),
        ];
    }
}
