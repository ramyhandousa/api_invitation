<?php

namespace App\Http\Resources\invitation;

use Illuminate\Http\Resources\Json\JsonResource;

class showSupervisorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->invitation->id,
            'name'                      => $this->invitation->name,
            'image'                     => $this->when( $this->invitation->image, env('APP_URL').'/'. $this->invitation->image),
            'date'                      => $this->invitation->date,
            'time_from'                 => $this->invitation->time_from,
            'time_to'                   => $this->invitation->time_to,
            'number_guests'             => $this->invitation->number_guests,
            'desc'                      => $this->invitation->desc,
            'latitude'                  => $this->invitation->latitude,
            'longitude'                 => $this->invitation->longitude,
            'address'                   => $this->invitation->address,
            'guest_login'               => (boolean) $this->guest_login,
            'add_invitees'              => (boolean) $this->add_invitees,
            'edit_invitation'           => (boolean) $this->edit_invitation,
            'status'                    => $this->status,
            'message'                   => $this->when($this->status == 'refuse' , $this->message),
        ];
    }
}
