<?php

namespace App\Http\Resources\invitation;

use Illuminate\Http\Resources\Json\JsonResource;

class filterRelation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'phone'     => $this->phone,
            'image'     => $this->image ? $this->image : optional($this->user)->image,
            $this->mergeWhen(  $this->type_user == 'guest',[
                'guest_status'              => $this->guest_status,
                'table_number'              =>  $this->when($this->table_number , (int) $this->table_number),
                'additional_information'    => $this->additional_information,

            ]),
            $this->mergeWhen(  $this->type_user == 'supervisor',[
                'guest_login'           => (boolean) $this->guest_login,
                'add_invitees'          => (boolean) $this->add_invitees,
                'edit_invitation'       => (boolean) $this->edit_invitation,
            ]),
            'status'    => $this->status,
            'message'    => $this->when($this->message ,$this->message ),
        ];
    }
}
