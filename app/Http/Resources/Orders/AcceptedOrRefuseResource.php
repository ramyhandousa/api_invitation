<?php

namespace App\Http\Resources\Orders;

use Illuminate\Http\Resources\Json\JsonResource;

class AcceptedOrRefuseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->invitation->id,
            'name'                      => $this->invitation->name,
            'image'                     => $this->when( $this->invitation->image, env('APP_URL').'/'. $this->invitation->image),
            'date'                      => $this->invitation->date,
            'time_from'                 => $this->invitation->time_from,
            'time_to'                   => $this->invitation->time_to,
            'number_guests'             => $this->invitation->number_guests,
            'desc'                      => $this->invitation->desc,
            'latitude'                  => $this->invitation->latitude,
            'longitude'                 => $this->invitation->longitude,
            'address'                   => $this->invitation->address,
            'guest_status'              => $this->guest_status,
            'table_number'              => (int) $this->table_number,
            'additional_information'    => $this->additional_information,
            'status'                    => $this->status,
            'message'                   => $this->when($this->status == 'refuse' , $this->message),
        ];
    }
}
