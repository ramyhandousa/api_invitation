<?php

namespace App\Http\Resources\Notification;

use Illuminate\Http\Resources\Json\JsonResource;

class InvitationNotifactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status = $this->status ;

        if ($status == 1 ){
            $title =  __("trans.status_invitation_accepted") ;
            $my_status = 'accepted';
         }elseif ($status == -1){
            $title =  __("trans.status_invitation_refuse");
            $my_status = 'refused';
        }else{
            $title = __("trans.status_invitation_waiting");
            $my_status = 'waiting';
        }

        return [
            'id'                => $this->id,
            'invitation_id'     => optional(optional($this->notification)->invitation)->id,
            'invitation_date'   => optional(optional($this->notification)->invitation)->date,
            'title'             => $title . $this->user->name   ,
            'time'              => $this->updated_at,
            'status'            => $my_status,
         ];
    }
}
