<?php

namespace App\Http\Resources\Notification;

use Illuminate\Http\Resources\Json\JsonResource;

class ListNotificationResource extends JsonResource
{

    public function toArray($request)
    {
        $user = $request->user();

       return [
           'id'                     => $this->id,
           'invitation_id'          => optional($this->invitation)->id,
           'invitation_date'        => optional($this->invitation)->date,
           'title'                  => $this->title,
           'body'                   => $this->body,
           'type'                   => $this->type,
           $this->mergeWhen($user->id == $this->user_id && $this->type == 3 ,[
               'un_read_count'      => $this->notify_invitation_un_read->count(),
               'invitation_count'   => $this->notify_invitation->count(),
               'invitation_last'    => InvitationNotifactionResource::collection($this->notify_invitation_last),
           ]),
           $this->mergeWhen($user->id != $this->user_id && $this->type == 3 && $this->my_status() ,[
               'status' => $this->when($this->my_status()  , (string) optional($this->my_status())->status )
           ]),

       ];
    }
}
