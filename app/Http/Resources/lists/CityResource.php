<?php

namespace App\Http\Resources\lists;

use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name_ar ? : $this->name_en
        ];
    }
}
