<?php

namespace App\Http\Resources\lists;

use Illuminate\Http\Resources\Json\JsonResource;

class BankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name_ar ? : $this->name_en,
            'number' => $this->number,
            'image' => $this->when($this->image, env('APP_URL') .'/' . $this->image),
        ];
    }
}
