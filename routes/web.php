<?php

/** @var \Laravel\Lumen\Routing\Router $router */


$router->get('/',function (){
   return 'master_api';
});

$router->group([
    'prefix' => 'v1'
], function () use ($router) {

    $router->group(['prefix' => 'Auth'], function () use ($router) {

        $router->post('login','AuthController@login');
        $router->post('sign-up', 'AuthController@register');
        $router->post('forgetPassword', 'AuthController@forgetPassword');
        $router->post('resetPassword', 'AuthController@resetPassword');
        $router->post('checkCode', 'AuthController@checkCodeActivation');
        $router->post('resendCode', 'AuthController@resendCode');
        $router->post('changPassword', 'AuthController@changPassword');
        $router->post('editProfile', 'AuthController@editProfile');
        $router->post('logOut','AuthController@logOut');
    });


    $router->group(['prefix' => 'invitation'], function () use ($router) {

        $router->get('/{id}/user','InvitationController@user');
        $router->get('/{id}','InvitationController@show');
        $router->get('/{id}/contacts','InvitationController@show_details');
        $router->post('/','InvitationController@store');
        $router->post('/supervisor/{id}','InvitationController@edit_invitation_by_supervisor');
        $router->post('/{id}','InvitationController@editInvitation');
        $router->post('/payonline/{id}','InvitationController@pay_invitation');
    });

    $router->post('Invitation/scan/{id}','InvitationController@scan_invitation');


    $router->get('/orderSupervisor','OrderSupervisorController@list');

    $router->get('/orderGuest','OrderGuestController@list');
    $router->post('/orderGuest/{id}/accepted','OrderGuestController@accepted');
    $router->post('/orderGuest/{id}/refuse','OrderGuestController@refuse');


    $router->get('cities','SettingController@listCities');
    $router->get('banks','SettingController@listBanks');
    $router->get('types','SettingController@listTypes');


    $router->get('list_notification','UserController@list_notification');
    $router->get('invitation_notify/{notification}','UserController@invitation_notifications');

    $router->group(['prefix' => 'setting'], function () use ($router) {

        $router->get('aboutUs','SettingController@aboutUs');
        $router->get('terms','SettingController@terms_user');
        $router->get('social','SettingController@social');
        $router->post('contact_us','SettingController@contact_us');
        $router->post('bank_transfer/{id}','SettingController@bank_transfer');
    });


    $router->group(['prefix' => 'payment'], function ()  use ($router) {

        $router->post('pay_online','PaymentController@pay_online');
        $router->get('success','PaymentController@success_payment');
        $router->get('error','PaymentController@error');

    });


});


$router->get('my_code',function (\Illuminate\Http\Request  $request){

    $phone = \App\Models\VerifyUser::wherePhone($request->phone)->first();

    if ($phone){
        return  $phone->action_code;
    }else{
        return 'غير موجود' ;
    }

});


$router->get('my_base_url',function (){
    $master_domain = env('APP_URL');

   $base =  app()->basePath();
    return $base;
});


$router->post('/send_sms',function (\Illuminate\Http\Request  $request){

    return \App\Libraries\twilio::sendMessage('bola',"+201006986069");
    return $request->all();
});
